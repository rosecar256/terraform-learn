provider "aws" {}

variable "cidr_blocks" {
  description = "cidr blocks and name tags for vpc and subnets"
  type = list(object({
    cidr_blok = string
    name = string
}))
  
}

variable avail_zone {}

# variable "subnet_cidr_block" {
#   description = "subnet cidr block"
#   default = "10.0.10.0/24"
  
# }

# variable "vpc_cidr_block" {
#   description = "vpc cidr block"
  
# }

# variable "environment" {
#   description = "deployment environment"
  
# }



resource "aws_vpc" "development-vpc" {
  cidr_block = var.cidr_blocks[0].cidr_blok
  tags = {
    Name: var.cidr_blocks[0].name
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = "${aws_vpc.development-vpc.id}"
  cidr_block = var.cidr_blocks[1].cidr_blok
  availability_zone = var.avail_zone
  tags = {
    Name: var.cidr_blocks[1].name
  }
}

# data "aws_vpc" "existing-vpc" {
#   default = true
# }

# resource "aws_subnet" "dev-subnet-2" {
#   vpc_id = "${data.aws_vpc.existing-vpc.id}"
#   cidr_block = "172.31.48.0/20"
#   availability_zone = "ap-southeast-1a"
#   tags = {
#     Name: "subnet-2-dev"
#   }
# }


# output "dev-vpc-id" {
#   value = "${aws_vpc.development-vpc.id}"
# }


# output "dev-subnet-id" {
#   value = "${aws_subnet.dev-subnet-1.id}"
# }

# region = "ap-southeast-1"
# access_key = "AKIAQWED3RTG6A6KQHPR"
# secret_key = "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"

  